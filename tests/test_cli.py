from unittest import TestCase
from src import cli


class TestCli(TestCase):

    def test_greet(self):
        self.assertEqual(cli.greet('Emir'), 'Hello, Emir')

    def test_farewell(self):
        self.assertEqual(cli.farewell('Emir'), 'Goodbye, Emir')
