#!/usr/bin/env python3.6
import argparse


def main():
    parser = argparse.ArgumentParser(description='Lets you collect all pull requests')
    parser.add_argument('--greet', '-g', help='Greet me', action='store_true')
    parser.add_argument('--name', '-n', help='Your name', type=str, default='World')
    args = parser.parse_args()

    # Do the stuff
    if args.greet:
        print(greet(args.name))
    else:
        print(farewell(args.name))


def greet(name):
    return 'Hello, {0}'.format(name)


def farewell(name):
    return 'Goodbye, {0}'.format(name)


if __name__ == '__main__':
    main()
